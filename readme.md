### Monarch Bali Cronjob Scheduller

PHP script untuk mendapatkan token channel aplikasi Monarch Bali

### Requirement
1. PHP version 7+
2. [Composer](https://getcomposer.org)

### Instalasi
1. Clone repo ini dengan ``git clone https://gitlab.com/bdsys/monarch-app-cron-job.git``
2. Masuk ke root project ``cd monarch-app-cron-job``
3. Install depedency dengan ``composer install`` atau ``php composer.phar install`` (tergantung installasi composer) 
4. Buat .env file ``cp .env.example .env``
5. Buka dan edit ``.env`` dengan text editor, kemudian isikan informasi database dan ototentikasi user channel yang digunakan
6. Buka crontab dengan mode edit dengan ``crontab -e``. Tambahkan code berikut untuk menjalankan scheduller setiap bulan: ``* * * 1-12 * php /path/ke/project/monarch-app-cron-job/index.php`` 

Done.
