<?php

require 'vendor/autoload.php';

$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

$host = getenv('DB_HOST');
$user = getenv('DB_USERNAME');
$password = getenv('DB_PASSWORD');
$database = getenv('DB_DATABASE');
$table = getenv('DB_TABLE');
$field = getenv('DB_FIELD');

$apiUsername = getenv('API_USERNAME');
$apiPassword = getenv('API_PASSWORD');

$con = mysqli_connect($host, $user, $password, $database);

if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    return;
}

$client = new \GuzzleHttp\Client();

$response = $client->request('POST', 'http://app.monarchbali.com/api/channel/login', [
    'form_params' => [
        'email' => $apiUsername,
        'password' => $apiPassword
    ]
]);

$code = $response->getStatusCode(); // 200
$reason = $response->getReasonPhrase(); // OK
$body = $response->getBody();

if($code == 200)
{
    $res = json_decode($body);
    if($res->status == 'success'){
        $sql = "UPDATE ".$table." SET ".$field."='".$res->data->token."'";

        if ($con->query($sql) === TRUE) {
            echo "Token: ".$res->data->token."\n";
            echo "Token updated successfully";
        } else {
            echo "Error updating token: " . $con->error;
        }
    } else {
        echo "Error: ". $res->message. "\n";
    }
} else {
    echo $body;
}




